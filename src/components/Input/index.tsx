import { useField } from '@unform/core'
import React, { useEffect, useRef } from 'react'
import { Label, Error } from '../Input/styles'
interface Iinput {
  label?: string
  name: any
  children?: any
  type?: string
  placeholder?: any
  value?: any
}

type InputProps = JSX.IntrinsicElements['input'] & Iinput

export default function InputElement({
  label,
  name,
  type,
  children,
  placeholder,
  value,
  ...rest
}: InputProps) {
  const ref = useRef<HTMLInputElement>(null)
  const { fieldName, registerField, defaultValue, error } = useField(name)

  const renderLabel = label || name

  useEffect(() => {
    if (!ref.current) return
    registerField({
      name: fieldName,
      ref: ref.current,
      path: 'value'
    })
  }, [ref.current, fieldName]) //eslint-disable-line

  return (
    <Label htmlFor={fieldName} style={{ marginTop: 10 }}>
      {error && <Error>{error}</Error>}
      <input
        name={fieldName}
        placeholder={placeholder}
        ref={ref}
        id={fieldName}
        type={type}
        aria-label={fieldName}
        defaultValue={value}
        {...rest}
      />
      <span>{renderLabel}</span>
    </Label>
  )
}
