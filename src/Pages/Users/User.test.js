import React from 'react'

import { fireEvent, render, waitForElement } from '@testing-library/react'

import Todo from '../Pages/CreateUser/index.tsx'

import {
  ApolloProvider,
  ApolloClient,
  HttpLink,
  InMemoryCache,
  split
} from '@apollo/client'
import { getMainDefinition } from '@apollo/client/utilities'
import { WebSocketLink } from '@apollo/link-ws'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

const GRAPHQL_ENDPOINT = 'tight-rooster-82.hasura.app/v1/graphql'

const httpLink = new HttpLink({
  uri: `https://${GRAPHQL_ENDPOINT}`
})

const wsLink = new WebSocketLink({
  uri: `ws://${GRAPHQL_ENDPOINT}`,
  options: {
    reconnect: true
  }
})

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  },
  wsLink,
  httpLink
)

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: splitLink
})

describe('Tests for Todo component', () => {
  it('Should add new task when form has been submitted', async () => {
    const { getByTestId, getByText } = render(
      <BrowserRouter>
        <ApolloProvider client={client}>
          <Switch>
            <Route path="/" component={Todo} />
          </Switch>
        </ApolloProvider>
      </BrowserRouter>
    )

    // const fieldNode = await waitForElement(() => getByTestId('form-field'))
    // const newTask = 'testing'
    // fireEvent.change(fieldNode, { target: { value: newTask } })
    // expect(fieldNode.value).toEqual(newTask)

    // const btnNode = await waitForElement(() => getByTestId('form-btn'))
    // fireEvent.click(btnNode)

    // const tdNode = await waitForElement(() => getByText(newTask))
    // expect(tdNode).toBeDefined()
  })
})
