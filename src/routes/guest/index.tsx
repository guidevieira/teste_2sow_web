import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { isAuthenticated } from '../../utils/login'

const GuestRoute = ({ component, ...rest }: any) => {
  const routeComponent = (props: any) =>
    !isAuthenticated() ? (
      React.createElement(component, props)
    ) : (
      <Redirect to={{ pathname: '/users' }} />
    )
  return <Route {...rest} render={routeComponent} />
}

export default GuestRoute
