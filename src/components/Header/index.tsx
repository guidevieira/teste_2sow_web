import React from 'react'
import { Container, Menu, Visibility, Image } from 'semantic-ui-react'
import { TOKEN_KEY } from '../../utils/login'
import history from '../../routes/history/index'
import { StyledLink } from './styles'

const Logo = () => {
  function logout() {
    localStorage.removeItem(TOKEN_KEY)
    history.push('/login')
  }
  return (
    <>
      <Visibility once={false}>
        <Menu borderless fixed={'top'} style={{ height: 60 }}>
          <Container text>
            <Menu.Item header>
              <Image
                size="tiny"
                src={'https://guidevieira.s3-us-west-1.amazonaws.com/2sow.png'}
              />
            </Menu.Item>

            <Menu.Menu position="right">
              <Menu.Item>
                <StyledLink to="/users" style={{ textDecoration: 'none' }}>
                  Usuários
                </StyledLink>
              </Menu.Item>
              <Menu.Item>
                <StyledLink
                  to="/create-user"
                  style={{ textDecoration: 'none' }}
                >
                  Inserir
                </StyledLink>
              </Menu.Item>
              <Menu.Item onClick={logout}>
                <span style={{ color: 'red' }}>Sair</span>
              </Menu.Item>
            </Menu.Menu>
          </Container>
        </Menu>
      </Visibility>
    </>
  )
}

export default Logo
