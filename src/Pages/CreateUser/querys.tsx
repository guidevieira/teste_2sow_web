import { gql } from '@apollo/client'

export const ADD_USER = gql`
  mutation(
    $name: String!
    $cpf: String!
    $email: String!
    $cep: String!
    $endereco: String!
    $numero: Int!
    $bairro: String!
    $cidade: String!
  ) {
    insert_users(
      objects: {
        cpf: $cpf
        email: $email
        name: $name
        cep: $cep
        endereco: $endereco
        numero: $numero
        bairro: $bairro
        cidade: $cidade
      }
    ) {
      returning {
        name
        email
        cpf
      }
    }
  }
`
