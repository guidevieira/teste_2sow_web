import React from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import CreateUser from '../Pages/CreateUser'
import Login from '../Pages/Login'
import Users from '../Pages/Users'

import history from '../routes/history'
import PrivateRoute from '../routes/private'
import GuestRoute from './guest'

const Routes: React.FC = () => (
  <Router history={history}>
    <Switch>
      <GuestRoute exact path={['/login', '/']} component={Login} />
      <PrivateRoute path="/create-user" component={CreateUser} />
      <PrivateRoute exact path="/users" component={Users} />
    </Switch>
  </Router>
)
export default Routes
