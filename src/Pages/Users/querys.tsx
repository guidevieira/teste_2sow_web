import { gql } from '@apollo/client'

export const FETCH = gql`
  subscription {
    users {
      id
      name
      cpf
      email
      cep
      endereco
      numero
      bairro
      cidade
    }
  }
`

export const DELETE = gql`
  mutation($id: uuid!) {
    delete_users(where: { id: { _eq: $id } }) {
      returning {
        name
        id
        email
      }
    }
  }
`

export const EDIT = gql`
  mutation(
    $id: uuid!
    $name: String
    $email: String
    $cpf: String
    $cep: String!
    $endereco: String!
    $numero: Int!
    $bairro: String!
    $cidade: String!
  ) {
    update_users(
      where: { id: { _eq: $id } }
      _set: {
        email: $email
        name: $name
        cpf: $cpf
        cep: $cep
        endereco: $endereco
        numero: $numero
        bairro: $bairro
        cidade: $cidade
      }
    ) {
      returning {
        id
      }
    }
  }
`
