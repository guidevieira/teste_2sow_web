import { FormHandles } from '@unform/core'
import React, { useRef } from 'react'
import Input from '../../components/Input/index'
import { Form } from '@unform/web'
import { TOKEN_KEY } from '../../utils/login'
import history from '../../routes/history'
import { Image } from 'semantic-ui-react'
import * as Yup from 'yup'
import { Container, ContainerImage } from './styles'

const Login: React.FC = () => {
  const formRef = useRef<FormHandles>(null)
  async function handleSubmit(data: any) {
    try {
      const schema = Yup.object().shape({
        email: Yup.string()
          .email('* Email invalido')
          .required('* O email é obrigatório'),
        senha: Yup.string()
          .required('* A senha é obrigatório')
          .test('len', '* Senha muito curta', (val: any) => val.length > 4)
      })
      await schema.validate(data, {
        abortEarly: false
      })

      localStorage.setItem(TOKEN_KEY, 'codigo jwt')
      history.push('/users')
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessage: any = {}

        err.inner.forEach(error => {
          errorMessage[error.path] = error.message
        })
        formRef.current!.setErrors(errorMessage)
      }
    }
  }
  return (
    <>
      <Container>
        <Form
          className="form"
          ref={formRef}
          onSubmit={handleSubmit}
          style={{ width: '20rem' }}
        >
          <ContainerImage>
            <Image
              width={100}
              src={'https://guidevieira.s3-us-west-1.amazonaws.com/2sow.png'}
            />
          </ContainerImage>
          <Input
            name="email"
            type="email"
            placeholder="Informe o email"
            label="Email"
          />
          <Input
            name="senha"
            type="password"
            placeholder="Informe a senha"
            label="Senha"
          />

          <button
            type="submit"
            className="fluid ui green button"
            style={{ marginTop: 20 }}
          >
            Entrar
          </button>
        </Form>
      </Container>
    </>
  )
}

export default Login
