import React from 'react'
import { GlobalStyles } from './GlobalStyles'
import Routes from './routes/index'

import {
  ApolloProvider,
  ApolloClient,
  HttpLink,
  InMemoryCache,
  split
} from '@apollo/client'
import { getMainDefinition } from '@apollo/client/utilities'
import { WebSocketLink } from '@apollo/link-ws'
import { BrowserRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

const GRAPHQL_ENDPOINT = 'tight-rooster-82.hasura.app/v1/graphql'

const httpLink = new HttpLink({
  uri: `https://${GRAPHQL_ENDPOINT}`
})

const wsLink = new WebSocketLink({
  uri: `ws://${GRAPHQL_ENDPOINT}`,
  options: {
    reconnect: true
  }
})

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  },
  wsLink,
  httpLink
)

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: splitLink
})

const App: React.FC = () => {
  return (
    <>
      <GlobalStyles />
      <ToastContainer />
      <BrowserRouter>
        <ApolloProvider client={client}>
          <Routes />
        </ApolloProvider>
      </BrowserRouter>
    </>
  )
}

export default App
