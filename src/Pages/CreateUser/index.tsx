import { useMutation } from '@apollo/client'
import React, { useState, useRef } from 'react'
import { Form } from '@unform/web'
import { FormHandles, Scope } from '@unform/core'
import Input from '../../components/Input/index'
import * as Yup from 'yup'
import history from '../../routes/history/index'
import Logo from '../../components/Header'
import ApiCep from '../../services/ViaCep'
import InputMask from '../../components/InputMask'
import { ADD_USER } from './querys'
import { toast } from 'react-toastify'

const Todo = () => {
  const formRef = useRef<FormHandles>(null)

  const errorCep = () => toast.error('CEP informado não encontrado')

  const [rua, setRua] = useState()
  const [cidades, setCidades] = useState()
  const [bairros, setBairros] = useState()

  async function fetchCep(event: any) {
    try {
      let cep: any = event.target.value
      cep = cep.replace('-', '')
      cep = cep.replace('_', '')

      if (cep.length != 8) return

      const response = await ApiCep.SearchCep(cep)

      if (response.data.erro) {
        toast.error('CEP informado não encontrado')
        return
      }

      let logradouro = response.data.logradouro
      let cidade = response.data.localidade
      let bairro = response.data.bairro

      setRua(logradouro)
      setCidades(cidade)
      setBairros(bairro)
    } catch (err) {
      toast.error('CEP informado não encontrado')
    }
  }

  async function handleSubmit(data: any) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required('* O nome é obrigatório'),
        email: Yup.string()
          .email('* Email invalido')
          .required('* O email é obrigatório'),
        cpf: Yup.string().required('* O cpf é obrigatório'),
        endereco: Yup.object().shape({
          cep: Yup.string().required('* O cep é obrigatório'),
          endereco: Yup.string().required('* O endereco é obrigatório'),
          numero: Yup.string().required('* O numero é obrigatório'),
          bairro: Yup.string().required('* O bairro é obrigatório'),
          cidade: Yup.string().required('* A cidade é obrigatório')
        })
      })
      await schema.validate(data, {
        abortEarly: false
      })

      await addReview({
        variables: {
          name: data.name,
          cpf: data.cpf,
          email: data.email,
          cep: data.endereco.cep,
          endereco: data.endereco.endereco,
          numero: data.endereco.numero,
          bairro: data.endereco.bairro,
          cidade: data.endereco.cidade
        }
      })
        .then(async (e: any) => {
          toast.success('Usuario cadastrado com sucesso')
          history.push('/users')
        })
        .catch(e => {
          toast.error('Erro ao cadastrar usuario')
          console.log(e)
        })
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessage: any = {}

        err.inner.forEach(error => {
          errorMessage[error.path] = error.message
        })
        formRef.current!.setErrors(errorMessage)
      }
    }
  }

  const [addReview] = useMutation(ADD_USER)

  return (
    <>
      <Logo />
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Form
          className="form"
          ref={formRef}
          onSubmit={handleSubmit}
          style={{ marginTop: 100, width: '20rem' }}
        >
          <Input
            name="name"
            type="text"
            placeholder="Informe o nome completo"
            label="Nome"
          />
          <Input name="email" type="text" placeholder="Informe o email" />
          <InputMask
            name="cpf"
            mask="999.999.999-99"
            label="Cpf"
            placeholder="Informe o cpf"
          />
          <Scope path="endereco">
            <InputMask
              name="cep"
              mask="99999-999"
              label="Cep"
              onChange={e => fetchCep(e)}
              placeholder="Informe o cep"
            />
            <Input
              name="endereco"
              placeholder="Informe o endereço"
              label="Endereço completo"
              value={rua}
            />
            <Input
              name="numero"
              type="number"
              placeholder="Informe o número"
              label="Número"
            />
            <Input
              name="bairro"
              type="text"
              placeholder="Informe o bairro"
              label="Bairro"
              value={bairros}
            />
            <Input
              name="cidade"
              type="text"
              placeholder="Informe a cidade"
              label="Cidade"
              value={cidades}
            />
          </Scope>
          <button className="fluid ui green button" style={{ marginTop: 25 }}>
            Salvar
          </button>
        </Form>
      </div>
    </>
  )
}

export default Todo
