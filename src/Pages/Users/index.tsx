import { useMutation, useSubscription } from '@apollo/client'
import React, { useState, useRef } from 'react'
import Input from '../../components/Input/index'
import { Table, Button, Modal, Dimmer, Loader } from 'semantic-ui-react'
import { ToastContainer, toast } from 'react-toastify'
import Logo from '../../components/Header'
import { Form } from '@unform/web'
import { FormHandles, Scope } from '@unform/core'
import { DELETE, EDIT, FETCH } from './querys'
import { Container } from './styles'

interface User {
  id: String
  name: string
  email: string
  cpf: string
  cep: string
  endereco: string
  numero: number
  bairro: string
  cidade: string
}

const Users = () => {
  const { loading, error, data } = useSubscription(FETCH)
  const [addReview] = useMutation(DELETE)
  const [editUser] = useMutation(EDIT)

  const [open, setOpen] = useState(false)
  const [user, setUser] = useState<User>({
    id: '',
    name: '',
    email: '',
    cpf: '',
    cep: '',
    endereco: '',
    numero: 0,
    bairro: '',
    cidade: ''
  })

  const formRef = useRef<FormHandles>(null)

  async function deleteUser(id: String) {
    await addReview({
      variables: { id: id }
    })
      .then(e => {
        toast.success('Deletado com sucesso')
      })
      .catch(e => {
        console.log(e)
      })
  }

  async function handleSubmit(data: any) {
    try {
      await editUser({
        variables: {
          id: user.id,
          name: data.name,
          email: data.email,
          cpf: data.cpf,
          cep: data.endereco.cep,
          endereco: data.endereco.endereco,
          numero: data.endereco.numero != '' ? data.endereco.numero : 0,
          bairro: data.endereco.bairro,
          cidade: data.endereco.cidade
        }
      })
        .then(e => {
          setOpen(false)
          toast.success('Editado com sucesso')
        })
        .catch(e => {
          console.log(e)
          toast.error('Erro')
        })
    } catch (err) {
      console.log(err)
      toast.error('Erro')
    }
  }

  function edit(user: any) {
    setUser(user)
    setOpen(true)
  }

  if (error) return <p>Error :(</p>
  return (
    <>
      <Logo />

      {loading == false ? (
        <Table celled style={{ padding: 100 }}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Nome</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Cpf</Table.HeaderCell>
              <Table.HeaderCell>Cidade</Table.HeaderCell>
              <Table.HeaderCell>Ações</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {data.users.map((t: any, index: number) => (
              <Table.Row key={index}>
                <Table.Cell>{t.name}</Table.Cell>
                <Table.Cell>{t.email}</Table.Cell>
                <Table.Cell>{t.cpf}</Table.Cell>
                <Table.Cell>{t.cidade}</Table.Cell>
                <Table.Cell>
                  <Button
                    className="mini"
                    icon="trash alternate"
                    color="red"
                    onClick={() => deleteUser(t.id)}
                  />
                  <Button
                    className="mini"
                    icon="pencil"
                    color="yellow"
                    onClick={() => edit(t)}
                  />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>

          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan="5"></Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      ) : (
        <Container>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Container>
      )}

      <ToastContainer />
      <Modal
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
      >
        <Modal.Header>Editar Usuário</Modal.Header>
        <Modal.Content>
          <Form className="form" ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="name"
              type="text"
              placeholder="Informe o nome completo"
              label="Nome"
              value={user.name}
            />
            <Input name="email" value={user.email} />
            <Input name="cpf" value={user.cpf} />
            <Scope path="endereco">
              <Input name="cep" value={user.cep} />
              <Input
                name="endereco"
                placeholder="Informe o endereço"
                label="Endereço completo"
                value={user.endereco}
              />
              <Input
                name="numero"
                type="number"
                placeholder="Informe o número"
                label="Número"
                value={user.numero}
              />
              <Input
                name="bairro"
                type="text"
                placeholder="Informe o bairro"
                label="Bairro"
                value={user.bairro}
              />
              <Input
                name="cidade"
                placeholder="Informe a cidade"
                label="Cidade"
                value={user.cidade}
              />
            </Scope>
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                marginTop: 15
              }}
            >
              <Button
                content="Salvar"
                type="submit"
                labelPosition="right"
                icon="checkmark"
                positive
              />
            </div>
          </Form>
        </Modal.Content>
      </Modal>
    </>
  )
}

export default Users
