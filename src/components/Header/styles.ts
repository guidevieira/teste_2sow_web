import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const StyledLink = styled(Link)`
  text-decoration: none;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`

export const Error = styled.span`
  color: #ff4d4f;
  font-weight: bold;
`

export const Label = styled.label`
  display: flex;
  flex-direction: column-reverse;
  position: relative;
  > span:first-child {
    color: #ff4d4f;
    line-height: 1.3;
    width: 100%;
    border-radius: 4px;
    transform: none;
    animation: fadeIn 350ms ease-in-out 1;
    @keyframes fadeIn {
      from {
        transform: translateY(-20px);
        opacity: 0;
      }
      to {
        transform: translateY(0);
        opacity: 1;
      }
    }
    + input,
    select {
      border-color: #ff4d4f;
      + svg {
        fill: #ff4d4f;
      }
    }
  }
`
