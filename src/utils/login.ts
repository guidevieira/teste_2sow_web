import history from '../routes/history'

export const TOKEN_KEY = '@teste2sow:jwt'
export const TOKEN_USER = '@teste2sow:user'

export const login = (token: string) => {
  localStorage.setItem(TOKEN_KEY, token)
}

export const logout = () => {
  localStorage.removeItem(TOKEN_KEY)
  history.push('/login')
}

export const isAuthenticated = () => {
  return !!localStorage.getItem(TOKEN_KEY)
}

export const getToken = (): string | null => localStorage.getItem(TOKEN_KEY)

export const getUniqueToken = () => {
  const token = localStorage.getItem(TOKEN_KEY)
  if (token !== null && token !== undefined && typeof token === 'string') {
    return token
  }
  return ''
}
