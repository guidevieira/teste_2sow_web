import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`

  .form {


    textarea {
    background: #fff;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    height: 38px;
    padding: 0 15px;
    color: rgba(0, 0, 0, 0.65);
    margin: 0 0 10px;
    width: 100%;
    display: inline-block;
    }

.input-file {
  background: none;
  border: none;
  padding:0
}

    input {
    background: #fff;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    height: 38px;
    padding: 0 15px;
    color: rgba(0, 0, 0, 0.65);
    /* margin: 0 0 10px; */
    width: 100%;
    display: inline-block;

    &::placeholder {
      color: #d9d9d9;
    }
    &:hover,
    &:focus {
      border-color: #1890ff;
    }
    }

    .checkbox {
      input[type="checkbox"] {
       height: 15px;
      }
      display: flex;
      flex-direction: column-reverse;
      align-items: center;
    }

    .choice {
      input[type="checkbox"] {
        width: 15px;
      }
      

      label {
        float: left;
        width: 97%;
        
      }
    }
  }
`
