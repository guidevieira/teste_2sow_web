import React from 'react'
import { render } from 'react-dom'
import App from './App'
import 'semantic-ui-css/semantic.min.css'
import 'react-toastify/dist/ReactToastify.css'

render(<App />, document.getElementById('root'))
